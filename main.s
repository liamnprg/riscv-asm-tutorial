.section .text
.globl _start
exit:
	#1 is the exit code, makefiles mess with this so when you check it it will be 2
	li a0,1
#	li a1, 1
	#93 is the exit syscall code, see next comment
	li a7,93
	ecall
	
_start:
	/*https://gitlab.com/strace/strace/blob/master/linux/64/syscallent.h */
	#1 is the file descriptor(stdout)
	li a0,1
	#create a pointer to msg
	lui a1,%hi(msg)
	addi a1,a1,%lo(msg)
	#length of msg
	li a2,5
	#write syscall number
	li a7,64
	ecall
	#jump
	j exit

.section .rodata
msg:
	.string "Hello World"
